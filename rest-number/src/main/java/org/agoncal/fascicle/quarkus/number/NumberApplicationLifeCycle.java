package org.agoncal.fascicle.quarkus.number;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class NumberApplicationLifeCycle {

  private static final Logger LOGGER = Logger.getLogger(NumberApplicationLifeCycle.class);

  void onStart(@Observes StartupEvent ev) {
    LOGGER.info("       _  ____  _   _ _____ ____  ");
    LOGGER.info("      | |/ __ \\| \\ | |_   _/ __ \\ ");
    LOGGER.info("      | | |  | |  \\| | | || |  | |");
    LOGGER.info("  _   | | |  | | . ` | | || |  | |");
    LOGGER.info(" | |__| | |__| | |\\  |_| || |__| |");
    LOGGER.info("  \\____/ \\____/|_| \\_|_____\\___\\_\\");
    LOGGER.info(" Powered by Quarkus");
    LOGGER.info("The application Number is starting with profile " + ProfileManager.getActiveProfile());
  }
  void onStop(@Observes ShutdownEvent ev) {
    LOGGER.info("The application Number is stopping...");
  }

}
